#!/usr/bin/env python3

from flask import Flask, request
from gevent.pywsgi import WSGIServer
from pymessenger.bot import Bot
import requests
import pdb
import json
import re
import urllib
import time
from  daemon import Daemon
import sys

app = Flask(__name__)

VERIFY_TOKEN ='MyFbBot'
ACCESS_TOKEN ='token'
API_URL      = 'API'
PID_FILE     = '/var/run/daemons/bot-ots-fb.pid'
IP_ADDR_SRV  = 'IPserver'
PORT_SRV     = 5050
CERT_FILE    ='/etc/letsencrypt/live/botest.top/fullchain.pem'
KEY_FILE     ='/etc/letsencrypt/live/botest.top/privkey.pem'

ORDER        = False
MY_ID        = 'ID'
PROPOSITION  = False

bot = Bot(ACCESS_TOKEN)

@app.route('/facebook', methods=['GET','POST'])
def receive_message():
    global PROPOSITION
    #log = open('/home/cel/bot_fb/log.txt', 'a')
    if request.method =='GET':
        token_sent = request.args['hub.verify_token']
        if token_sent == VERIFY_TOKEN:
            return request.args['hub.challenge']
    else:
        #pdb.set_trace()
        #log.write('\nНовий запрос\n')
        output = request.get_json()
        #log.write(json.dumps(output))
        for event in output['entry']:
            messaging = event['messaging']
            for mess in messaging:
                #визначаємо ID, щоб знати куди відправляти відповідь
                recipient_id = mess['sender']['id']
                if mess.get('message'):
                    #якщо користувач відправив GIF, фото, відео чи інший не текстовий об'єкт
                    if mess['message'].get('attachments'):
                        response_sent_nontext = "Ой!!! Прийшло щось незрозуміле! :)"
                        bot.send_text_message(recipient_id, response_sent_nontext)
                        is_next(recipient_id)
                        continue
                    if mess['message'].get('quick_reply'):
                        if mess['message']['quick_reply'].get('payload')=='GET_CONTACTS':
                            send_contacts(recipient_id)
                        elif mess['message']['quick_reply'].get('payload')=='GET_ORDER_INFO':
                            send_order_info(recipient_id)
                        elif mess['message']['quick_reply'].get('payload')=='GET_WORKS_TIME':
                            send_works_time(recipient_id)
                        elif mess['message']['quick_reply'].get('payload')=='SET_PROPOSITION':
                            PROPOSITION = True
                            resp_text = "Напишіть будь ласка свою пропозицію, і ми її обов'язково проаналізуємо. :)"
                            bot.send_text_message(recipient_id, resp_text)
                        elif mess['message']['quick_reply'].get('payload')=='SET_YES':
                            show_menu(recipient_id, mess_text='Ok. Оберіть бажану дію. :)')
                        elif mess['message']['quick_reply'].get('payload')=='SET_NO':
                            resp_text = 'Радий був допомогти. :)\nЯкщо щось знадобиться - просто привітайтесь зі мною.'
                            bot.send_text_message(recipient_id, resp_text)
                        continue
                    if mess['message'].get('text'):
                        log = open('/home/cel/bot_fb/log.txt', 'a')
                        log_record = {'text': mess['message']['text'], 'id': recipient_id, 'time':time.asctime()}
                        log.write(str(log_record)+',\n')
                        log.close()
                        response_sent_text = mess['message']['text']
                        analyse_message(recipient_id, response_sent_text)
                        continue
        #log.close()
        return "Message Processed"

def analyse_message(recipient_id, mess_text):
    global ORDER, PROPOSITION

    if PROPOSITION:
        PROPOSITION = False
        txt = 'Пропозиція від ' + recipient_id + '\n'+mess_text
        log = open('/home/cel/bot_fb/proposition.txt', 'a')
        log_record = {'text': mess_text, 'id': recipient_id, 'time':time.asctime()}
        log.write(str(log_record)+',\n')
        log.close()
        bot.send_text_message(MY_ID, txt)
        bot.send_text_message(recipient_id, 'Дякую! :)\nЯ передам вашу думку керівництву.')
        is_next(recipient_id)
        return 'Success'
    mess_text = mess_text.upper()
    rr = re.search(r'\bПРИВІТ', mess_text)
    if rr:
        text = the_part_of_day() + "\nЯ бот компанії 'ОфісТехСервіс'. :)\n Оберіть бажану дію."
        show_menu(recipient_id, mess_text=text)
        return 'Success'
    if ORDER:
        ORDER = False
        txt = mess_text.replace('K', 'К').replace('B', 'В')
        rr = re.search(r'\bКВ-\d{7}', txt)
        if rr:
            query= API_URL+'/'+urllib.parse.quote(rr[0])+'/123'
            resp = requests.get(query)
            if resp.status_code == 200:
                resp_text = resp.json()['status']
            else:
                resp_text = 'Сталася помилка під час обробки запиту. :( Спробуйте знову.'
            bot.send_text_message(recipient_id, resp_text)
            is_next(recipient_id)
            return 'Success'
        else:
            resp_text = "Введене вами значення не є номером квитанції :(\nФормат номера квитанції 'КВ-0000000'"
            bot.send_text_message(recipient_id, resp_text)
            is_next(recipient_id)
            return 'Success'
    else:
        resp_text = "Напишіть мені 'Привіт', і я вам підкажу. :)"
        bot.send_text_message(recipient_id, resp_text)
        return 'Success'

def the_part_of_day():
    time_struct = time.localtime(time.time())
    hour = time_struct.tm_hour
    if hour < 4:
        result = 'Доброї ночі!'
    elif hour <=10:
        result = 'Доброго ранку!'
    elif hour <=17:
        result = 'Доброго дня!'
    elif hour <=22:
        result = 'Доброго вечора!'
    else:
        result = 'Доброї ночі!'
    return result

def show_menu(recipient_id, mess_text=''):
    qreplies=[]
    qreplies.append({"content_type":"text","title":"Перебіг ремонту","payload":"GET_ORDER_INFO"})
    qreplies.append({"content_type":"text","title":"Контакти","payload":"GET_CONTACTS"})
    qreplies.append({"content_type":"text","title":"Графік роботи","payload":"GET_WORKS_TIME"})
    qreplies.append({"content_type":"text","title":"Побажання, пропозиції та відгуки","payload":"SET_PROPOSITION"})
    meesage = {"text":mess_text, "quick_replies":qreplies}
    bot.send_message(recipient_id,meesage)
    return 'Success'

def is_next(recipient_id):
    qreplies=[]
    qreplies.append({"content_type":"text","title":"Так","payload":"SET_YES"})
    qreplies.append({"content_type":"text","title":"Ні","payload":"SET_NO"})
    meesage = {"text":'У вас ще є запитання?', "quick_replies":qreplies}
    bot.send_message(recipient_id,meesage)
    return 'Success'

def send_contacts(recipient_id):
    buttons = []
    buttons.append({"type":"phone_number", "title":"(067)473-62-69","payload":"+380674736269"})
    buttons.append({"type":"phone_number", "title":"(04744)3-44-07","payload":"+380474434407"})
    buttons.append({"type":"phone_number", "title":"(04744)3-98-74","payload":"+380474439874"})
    bot.send_button_message(recipient_id, "Ось наші телефони:", buttons)
    is_next(recipient_id)
    return 'Success'

def send_order_info(recipient_id):
    global ORDER
    ORDER = True
    bot.send_text_message(recipient_id, 'Введіть номер квитанції')
    return 'Success'

def send_works_time(recipient_id):
    bot.send_text_message(recipient_id, 'Пн-Пт      9:00-17:00\nСб            10:00-14:00\nНеділя     Вихідний\n\nА я відповідаю на питання цілодобово! :)')
    is_next(recipient_id)
    return 'Success'

class MyDaemon(Daemon):
    def run(self):
        http_server = WSGIServer((IP_ADDR_SRV, PORT_SRV), app, certfile=CERT_FILE, keyfile=KEY_FILE)
        http_server.serve_forever()

if __name__ == '__main__':
    myDaemon = MyDaemon(PID_FILE)
    if len(sys.argv) ==2:
        if 'start'==sys.argv[1]:
            myDaemon.start()
        elif 'stop'==sys.argv[1]:
            myDaemon.stop()
        elif 'restart'==sys.argv[1]:
            myDaemon.restart()
        else:
            print("Unknown command")
            print("usage: %s start|stop|restart" % sys.argv[0])
            sys.exit(2)
        sys.exit(0)
    else:
        print("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)
